﻿using ComputationalGeometry.Algorithm;
using ComputationalGeometry.Command;
using ComputationalGeometry.Geometry;
using ComputationalGeometry.Randomizer;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace ComputationalGeometry.ViewModel
{
	class VoronoiDiagramViewModel : PageViewModel
	{
		private readonly GeometryRandomizer _randomizer;
		private VoronoiDiagramAlgorithm _algorithm;
		private ObservableCollection<LineSegmentViewModel> _voronoiDiagramLineSegments;
		private ICommand _findVoronoiDiagramCommand;
		private WriteableBitmapViewModel _bitmap;

		public override string Name => "Voronoi Diagram";
		public int Height { get; set; } = 800;
		public int Width { get; set; } = 1000;
		public int PointsAmount { get; set; } = 1000;
		public int BorderOffset { get; set; } = 100;

		public ObservableCollection<LineSegmentViewModel> VoronoiDiagramLineSegments
		{
			get
			{
				return _voronoiDiagramLineSegments;
			}
			set
			{
				_voronoiDiagramLineSegments = value;

				OnPropertyChanged(nameof(VoronoiDiagramLineSegments));
			}
		}
		public WriteableBitmap Bitmap
		{
			get
			{
				return _bitmap.Bitmap;
			}
		}

		public VoronoiDiagramViewModel()
		{
			_randomizer = new GeometryRandomizer(Height, Width);
			_bitmap = new WriteableBitmapViewModel(Height, Width);
		}

		public ICommand FindVoronoiDiagramCommand
		{
			get
			{
				return _findVoronoiDiagramCommand ?? (_findVoronoiDiagramCommand = new RelayCommand(obj =>
				{
					_algorithm = new DivideAndConquerVoronoiDiagramAlgorithm(Height, Width);
					_algorithm.Points = new Collection<Point>(
						_randomizer.NextPoints(PointsAmount).ToList());

					VoronoiDiagramLineSegments = new ObservableCollection<LineSegmentViewModel>(
						_algorithm.Run().Select(l => new LineSegmentViewModel(l)).ToList());

					_bitmap.InitializePixels(_algorithm.Points);
				}));
			}
		}
	}
}
