﻿using ComputationalGeometry.Geometry;

namespace ComputationalGeometry.ViewModel
{
	class RectangleViewModel : BaseViewModel
	{
		private readonly Rectangle _rectangle;

		public RectangleViewModel(Rectangle rectangle)
		{
			_rectangle = rectangle;
		}

		public double Left
		{
			get
			{
				return _rectangle.X1;
			}
			set
			{
				_rectangle.X2 = value + Width;
				_rectangle.X1 = value;

				OnPropertyChanged(nameof(Left));
			}
		}

		public double Top
		{
			get
			{
				return _rectangle.Y1;
			}
			set
			{
				_rectangle.Y2 = value + Height;
				_rectangle.Y1 = value;

				OnPropertyChanged(nameof(Top));
			}
		}

		public double Height
		{
			get
			{
				return _rectangle.Y2 - _rectangle.Y1;
			}
			set
			{
				_rectangle.Y2 = Top + value;

				OnPropertyChanged(nameof(Height));
			}
		}

		public double Width
		{
			get
			{
				return _rectangle.X2 - _rectangle.X1;
			}
			set
			{
				_rectangle.X2 = Left + value;

				OnPropertyChanged(nameof(Width));
			}
		}
	}
}
