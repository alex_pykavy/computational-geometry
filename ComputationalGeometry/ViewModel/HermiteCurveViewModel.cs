﻿using ComputationalGeometry.Algorithm;
using ComputationalGeometry.Command;
using ComputationalGeometry.Geometry;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace ComputationalGeometry.ViewModel
{
	class HermiteCurveViewModel : PageViewModel
	{
		private readonly HermiteCurveAlgorithm _algorithm = new HermiteCurveAlgorithm();
		private ICommand _clearCanvasCommand, _drawCurveCommand;
		private ObservableCollection<LineSegmentViewModel> _curveLineSegments = new ObservableCollection<LineSegmentViewModel>();
		private ObservableCollection<PointViewModel> _points = new ObservableCollection<PointViewModel>();

		public override string Name => "Hermite Curve";
		public int Height { get; set; } = 800;
		public int Width { get; set; } = 1000;

		public ObservableCollection<PointViewModel> Points
		{
			get
			{
				return _points;
			}
			set
			{
				_points = value;

				OnPropertyChanged(nameof(Points));
			}
		}
		public ObservableCollection<LineSegmentViewModel> CurveLineSegments
		{
			get
			{
				return _curveLineSegments;
			}
			set
			{
				_curveLineSegments = value;

				OnPropertyChanged(nameof(CurveLineSegments));
			}
		}

		public ICommand ClearCanvasCommand
		{
			get
			{
				return _clearCanvasCommand ?? (_clearCanvasCommand = new RelayCommand(obj =>
				{
					Points = new ObservableCollection<PointViewModel>();

					CurveLineSegments = new ObservableCollection<LineSegmentViewModel>();
				}));
			}
		}

		public ICommand DrawCurveCommand
		{
			get
			{
				return _drawCurveCommand ?? (_drawCurveCommand = new RelayCommand(obj =>
				{
					DrawCurve();

					Points.CollectionChanged += (s, e) => DrawCurve();
				}));
			}
		}

		private void DrawCurve()
		{
			if (Points.Count < 4)
			{
				System.Windows.MessageBox.Show($"There should be at least 4 control points. But got {Points.Count}");

				return;
			}

			List<Point> controlPoints = Points.Skip(Math.Max(0, Points.Count - 4))
													  .Select(p => new Point { X = p.X, Y = p.Y })
													  .ToList();
			List<Point> curvePoints = _algorithm.Run(controlPoints).ToList();

			List<LineSegment> curveLineSegmentsList = new List<LineSegment>();
			Point previousPoint = curvePoints.First();
			foreach (Point currentPoint in curvePoints.Skip(1))
			{
				curveLineSegmentsList.Add(new LineSegment { Start = previousPoint, End = currentPoint });

				previousPoint = currentPoint;
			}

			CurveLineSegments = new ObservableCollection<LineSegmentViewModel>(
				curveLineSegmentsList.Select(l => new LineSegmentViewModel(l)).ToList());
		}
	}
}
