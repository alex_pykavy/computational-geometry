﻿using ComputationalGeometry.Geometry;
using System.Collections.Generic;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ComputationalGeometry.ViewModel
{
	class WriteableBitmapViewModel : BaseViewModel
	{
		private readonly uint[] _pixels;
		List<Point> _initializedPoints = new List<Point>();
		List<Point> _previousHighlightedPoints = new List<Point>();

		public WriteableBitmap Bitmap { get; }
		public int Height { get; }
		public int Width { get; }

		public WriteableBitmapViewModel(int height, int width)
		{
			_pixels = new uint[height * width];

			Bitmap = new WriteableBitmap(width, height, 96, 96, PixelFormats.Bgra32, null);
			Height = height;
			Width = width;
		}

		public void InitializePixels(IEnumerable<Point> points)
		{
			ColorPixels(_initializedPoints, 0xffffffff);
			_initializedPoints.Clear();

			ColorPixels(points, 0xff000000);
			_initializedPoints.AddRange(points);

			_previousHighlightedPoints.Clear();
		}

		public void HighlightPixels(IEnumerable<Point> points)
		{
			ColorPixels(points, 0xffff0000);

			_previousHighlightedPoints.AddRange(points);
		}

		public void UnhighlightPixels()
		{
			ColorPixels(_previousHighlightedPoints, 0xff000000);

			_previousHighlightedPoints.Clear();
		}

		private void ColorPixels(IEnumerable<Point> points, uint color)
		{
			foreach (Point p in points)
			{
				_pixels[Width * (int)p.Y + (int)p.X] = color;
			}

			Bitmap.WritePixels(new System.Windows.Int32Rect(0, 0, Width, Height), _pixels, Width * 4, 0);
			OnPropertyChanged(nameof(Bitmap));
		}
	}
}
