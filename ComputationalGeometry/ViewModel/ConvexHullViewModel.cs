﻿using ComputationalGeometry.Algorithm;
using ComputationalGeometry.Command;
using ComputationalGeometry.Geometry;
using ComputationalGeometry.Randomizer;
using MathNet.Numerics.Distributions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace ComputationalGeometry.ViewModel
{
	class ConvexHullViewModel : PageViewModel
	{
		private readonly GeometryRandomizer _randomizer;
		private ConvexHullAlgorithm _algorithm;
		private ObservableCollection<LineSegmentViewModel> _convexHullLineSegments;
		private ICommand _findConvexHullCommand;
		private WriteableBitmapViewModel _bitmap;

		public override string Name => "Convex Hull";
		public int Height { get; set; } = 800;
		public int Width { get; set; } = 1000;
		public int PointsAmount { get; set; } = 1000;
		public int BorderOffset { get; set; } = 100;

		public ObservableCollection<LineSegmentViewModel> ConvexHullLineSegments
		{
			get
			{
				return _convexHullLineSegments;
			}
			set
			{
				_convexHullLineSegments = value;

				OnPropertyChanged(nameof(ConvexHullLineSegments));
			}
		}
		public WriteableBitmap Bitmap
		{
			get
			{
				return _bitmap.Bitmap;
			}
		}

		public ConvexHullViewModel()
		{
			_randomizer = new GeometryRandomizer(Height, Width);
			_bitmap = new WriteableBitmapViewModel(Height, Width);
		}

		public ICommand FindConvexHullCommand
		{
			get
			{
				return _findConvexHullCommand ?? (_findConvexHullCommand = new RelayCommand(obj =>
				{
					_algorithm = new GrahamScanAlgorithm(Height, Width);
					_algorithm.Points = new Collection<Point>(
						NextNormalDistributedPoints(PointsAmount, 0, Width, 0, Height).ToList());

					ConvexHullLineSegments = new ObservableCollection<LineSegmentViewModel>();

					IEnumerable<Point> convexHull = _algorithm.Run();
					Point startPoint = convexHull.First();
					foreach (Point endPoint in convexHull.Skip(1).Concat(convexHull.Take(1)))
					{
						ConvexHullLineSegments.Add(new LineSegmentViewModel(new LineSegment { Start = startPoint, End = endPoint }));

						startPoint = endPoint;
					}

					_bitmap.InitializePixels(_algorithm.Points);
				}));
			}
		}

		private IEnumerable<Point> NextNormalDistributedPoints(int amount, double minX, double maxX, double minY, double maxY)
		{
			for (int i = 0; i < amount; i++)
			{
				yield return new Point
				{
					X = NextNormalDouble(minX, maxX),
					Y = NextNormalDouble(minY, maxY)
				};
			}
		}

		private double NextNormalDouble(double min, double max)
		{
			double sample = Normal.Sample(0, 0.3);
			while (Math.Abs(sample) > 1)
			{
				sample = Normal.Sample(0, 1);
			}

			double ratio = (max - min) / 2;
			double center = min + ratio;

			return center + ratio * sample;
		}
	}
}
