﻿using ComputationalGeometry.Algorithm;
using ComputationalGeometry.Randomizer;
using System.Collections.ObjectModel;
using System.Windows.Input;
using ComputationalGeometry.Command;
using ComputationalGeometry.Geometry;
using System.Linq;
using System.Windows.Media.Imaging;

namespace ComputationalGeometry.ViewModel
{
	class LocalizationViewModel : PageViewModel
	{
		private readonly GeometryRandomizer _randomizer;
		private LocalizationAlgorithm _algorithm;
		private RectangleViewModel _rectangle;
		private ICommand _generatePointsCommand;
		private WriteableBitmapViewModel _bitmap;
		private bool _doUseQuadTreeAlgorithm;

		public override string Name => "Localization";
		public int Height { get; set; } = 1000;
		public int Width { get; set; } = 1000;
		public int PointsAmount { get; set; } = 1000;
		public int QuadTreeMaxNodeCapacity { get; set; } = 3;
		public bool DoUseQuadTreeAlgorithm
		{
			get
			{
				return _doUseQuadTreeAlgorithm;
			}
			set
			{
				_doUseQuadTreeAlgorithm = value;

				OnPropertyChanged(nameof(DoUseQuadTreeAlgorithm));
			}
		}
		public WriteableBitmap Bitmap
		{
			get
			{
				return _bitmap.Bitmap;
			}
		}
		public RectangleViewModel Rectangle
		{
			get
			{
				return _rectangle;
			}
			set
			{
				_rectangle = value;

				OnPropertyChanged(nameof(Rectangle));
			}
		}

		public LocalizationViewModel()
		{
			_randomizer = new GeometryRandomizer(Height, Width);
			_bitmap = new WriteableBitmapViewModel(Height, Width);
		}

		public ICommand GeneratePointsCommand
		{
			get
			{
				return _generatePointsCommand ?? (_generatePointsCommand = new RelayCommand(obj =>
				{
					if (DoUseQuadTreeAlgorithm)
					{
						_algorithm = new QuadTreeAlgorithm(QuadTreeMaxNodeCapacity, Height, Width);
					}
					else
					{
						_algorithm = new StupidLocalizationAlgorithm(Height, Width);
					}
					_algorithm.Points = new Collection<Point>(_randomizer.NextPoints(PointsAmount).ToList());
					_algorithm.Region = new Rectangle();
					_algorithm.Prepare();

					_bitmap.InitializePixels(_algorithm.Points);

					Rectangle = new RectangleViewModel(_algorithm.Region);
					Rectangle.PropertyChanged += (s, e) =>
					{
						_bitmap.UnhighlightPixels();
						_bitmap.HighlightPixels(_algorithm.Run());
					};
				}));
			}
		}
	}
}
