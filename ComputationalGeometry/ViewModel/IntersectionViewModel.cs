﻿using ComputationalGeometry.Algorithm;
using ComputationalGeometry.Command;
using ComputationalGeometry.Geometry;
using ComputationalGeometry.Randomizer;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace ComputationalGeometry.ViewModel
{
	class IntersectionViewModel : PageViewModel
	{
		private readonly GeometryRandomizer _randomizer;
		private IntersectionAlgorithm _algorithm;
		private Collection<LineSegment> _generagedLineSegments;
		private ObservableCollection<LineSegmentViewModel> _lineSegments;
		private ICommand _generateLineSegmentsCommand;
		private ICommand _findIntersectionsCommand;
		private WriteableBitmapViewModel _bitmap;
		private bool _areSegmentsHidden = false;

		public override string Name => "Intersection";
		public int Height { get; set; } = 1000;
		public int Width { get; set; } = 1000;
		public int LineSegmentsAmount { get; set; } = 1000;
		public double LineSegmentsMinLength { get; set; } = 10;
		public double LineSegmentsMaxLength { get; set; } = 50;
		public bool DoUseBentleyOttmannAlgorithm { get; set; } = false;
		public bool AreSegmentsHidden
		{
			get
			{
				return _areSegmentsHidden;
			}
			set
			{
				_areSegmentsHidden = value;

				OnPropertyChanged(nameof(AreSegmentsHidden));
				OnPropertyChanged(nameof(LineSegmentsVisibility));
			}
		}
		public System.Windows.Visibility LineSegmentsVisibility => AreSegmentsHidden ? System.Windows.Visibility.Hidden : System.Windows.Visibility.Visible;

		public ObservableCollection<LineSegmentViewModel> LineSegments
		{
			get
			{
				return _lineSegments;
			}
			set
			{
				_lineSegments = value;

				OnPropertyChanged(nameof(LineSegments));
			}
		}
		public WriteableBitmap Bitmap
		{
			get
			{
				return _bitmap.Bitmap;
			}
		}

		public IntersectionViewModel()
		{
			_randomizer = new GeometryRandomizer(Height, Width);
			_bitmap = new WriteableBitmapViewModel(Height, Width);
		}

		public ICommand GenerateLineSegmentsCommand
		{
			get
			{
				return _generateLineSegmentsCommand ?? (_generateLineSegmentsCommand = new RelayCommand(obj =>
				{
					AreSegmentsHidden = false;

					_generagedLineSegments = new Collection<LineSegment>(_randomizer.NextLineSegments(
							LineSegmentsAmount,
							LineSegmentsMinLength,
							LineSegmentsMaxLength).ToList());

					LineSegments = new ObservableCollection<LineSegmentViewModel>(_generagedLineSegments.Select(l => new LineSegmentViewModel(l)).ToList());

					_bitmap.InitializePixels(Enumerable.Empty<Point>());
				},
				obj => LineSegmentsAmount <= 100000));
			}
		}

		public ICommand FindIntersectionsCommand
		{
			get
			{
				return _findIntersectionsCommand ?? (_findIntersectionsCommand = new RelayCommand(obj =>
				{
					AreSegmentsHidden = false;

					if (DoUseBentleyOttmannAlgorithm)
					{
						_algorithm = new BentleyOttmannAlgorithm();
					}
					else
					{
						_algorithm = new StupidIntersectionAlgorithm();
					}
					_algorithm.LineSegments = _generagedLineSegments;

					List<Point> intersectionPoints = _algorithm.Run().ToList();

					_bitmap.InitializePixels(intersectionPoints);
					_bitmap.HighlightPixels(intersectionPoints);
				}));
			}
		}
	}
}
