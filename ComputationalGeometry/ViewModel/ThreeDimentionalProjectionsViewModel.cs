﻿using ComputationalGeometry.Algorithm;
using ComputationalGeometry.Command;
using ComputationalGeometry.Geometry;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;

namespace ComputationalGeometry.ViewModel
{
	class ThreeDimentionalProjectionsViewModel : PageViewModel
	{
		private ThreeDimentionalProjectionsAlgorithm _algorithm;
		private ObservableCollection<LineSegmentViewModel> _projectionLineSegments;
		private CubeViewModel _cubeViewModel;
		private int _xAngle, _yAngle, _zAngle;
		private string _cubePositions, _triangleIndices;
		private double _projectionOffset = 200;
		private ICommand _drawCubeCommand;

		public override string Name => "3D Projections";
		public int Height { get; set; } = 800;
		public int Width { get; set; } = 1000;
		public int HalfWidth => Width / 2;
		public ObservableCollection<LineSegmentViewModel> ProjectionLineSegments
		{
			get
			{
				return _projectionLineSegments;
			}
			set
			{
				_projectionLineSegments = value;

				OnPropertyChanged(nameof(ProjectionLineSegments));
			}
		}
		public string CubePositions
		{
			get
			{
				return _cubePositions;
			}
			set
			{
				_cubePositions = value;

				OnPropertyChanged(nameof(CubePositions));
			}
		}
		public double ProjectionOffset
		{
			get
			{
				return _projectionOffset;
			}
			set
			{
				_projectionOffset = value;

				OnPropertyChanged(nameof(ProjectionOffset));
			}
		}
		public string TriangleIndices
		{
			get
			{
				return _triangleIndices;
			}
			set
			{
				_triangleIndices = value;

				OnPropertyChanged(nameof(TriangleIndices));
			}
		}
		public int XAngle
		{
			get
			{
				return _xAngle;
			}
			set
			{
				RotateCube(value - XAngle, 0, 0);

				_xAngle = value;

				OnPropertyChanged(nameof(XAngle));
			}
		}
		public int YAngle
		{
			get
			{
				return _yAngle;
			}
			set
			{
				RotateCube(0, value - YAngle, 0);

				_yAngle = value;

				OnPropertyChanged(nameof(YAngle));
			}
		}
		public int ZAngle
		{
			get
			{
				return _zAngle;
			}
			set
			{
				RotateCube(0, 0, value - ZAngle);

				_zAngle = value;

				OnPropertyChanged(nameof(ZAngle));
			}
		}

		public ICommand DrawCubeCommand
		{
			get
			{
				return _drawCubeCommand ?? (_drawCubeCommand = new RelayCommand(obj =>
				{
					double edgeLength = 50;

					_algorithm = new ThreeDimentionalProjectionsAlgorithm
					{
						Cube = new Cube
						{
							TriangleFacets = new TriangleFacet[]
							{
								new TriangleFacet
								{
									Points = new Point3D[]
									{
										new Point3D { X = 0, Y = 0, Z = 0 },
										new Point3D { X = edgeLength, Y = edgeLength, Z = 0 },
										new Point3D { X = edgeLength, Y = 0, Z = 0 }
									}
								},
								new TriangleFacet
								{
									Points = new Point3D[]
									{
										new Point3D { X = 0, Y = 0, Z = 0 },
										new Point3D { X = 0, Y = edgeLength, Z = 0 },
										new Point3D { X = edgeLength, Y = edgeLength, Z = 0 }
									}
								},
								new TriangleFacet
								{
									Points = new Point3D[]
									{
										new Point3D { X = edgeLength, Y = edgeLength, Z = 0 },
										new Point3D { X = 0, Y = edgeLength, Z = 0 },
										new Point3D { X = 0, Y = edgeLength, Z = edgeLength }
									}
								},
								new TriangleFacet
								{
									Points = new Point3D[]
									{
										new Point3D { X = edgeLength, Y = edgeLength, Z = 0 },
										new Point3D { X = 0, Y = edgeLength, Z = edgeLength },
										new Point3D { X = edgeLength, Y = edgeLength, Z = edgeLength }
									}
								},
								new TriangleFacet
								{
									Points = new Point3D[]
									{
										new Point3D { X = edgeLength, Y = 0, Z = 0 },
										new Point3D { X = edgeLength, Y = edgeLength, Z = 0 },
										new Point3D { X = edgeLength, Y = edgeLength, Z = edgeLength }
									}
								},
								new TriangleFacet
								{
									Points = new Point3D[]
									{
										new Point3D { X = edgeLength, Y = 0, Z = 0 },
										new Point3D { X = edgeLength, Y = edgeLength, Z = edgeLength },
										new Point3D { X = edgeLength, Y = 0, Z = edgeLength }
									}
								},
								new TriangleFacet
								{
									Points = new Point3D[]
									{
										new Point3D { X = 0, Y = 0, Z = 0 },
										new Point3D { X = 0, Y = 0, Z = edgeLength },
										new Point3D { X = 0, Y = edgeLength, Z = edgeLength }
									}
								},
								new TriangleFacet
								{
									Points = new Point3D[]
									{
										new Point3D { X = 0, Y = 0, Z = 0 },
										new Point3D { X = 0, Y = edgeLength, Z = edgeLength },
										new Point3D { X = 0, Y = edgeLength, Z = 0 },
									}
								},
								new TriangleFacet
								{
									Points = new Point3D[]
									{
										new Point3D { X = edgeLength, Y = edgeLength, Z = edgeLength },
										new Point3D { X = 0, Y = edgeLength, Z = edgeLength },
										new Point3D { X = 0, Y = 0, Z = edgeLength }
									}
								},
								new TriangleFacet
								{
									Points = new Point3D[]
									{
										new Point3D { X = edgeLength, Y = edgeLength, Z = edgeLength },
										new Point3D { X = 0, Y = 0, Z = edgeLength },
										new Point3D { X = edgeLength, Y = 0, Z = edgeLength }
									}
								},
								new TriangleFacet
								{
									Points = new Point3D[]
									{
										new Point3D { X = 0, Y = 0, Z = 0 },
										new Point3D { X = edgeLength, Y = 0, Z = edgeLength },
										new Point3D { X = 0, Y = 0, Z = edgeLength }
									}
								},
								new TriangleFacet
								{
									Points = new Point3D[]
									{
										new Point3D { X = 0, Y = 0, Z = 0 },
										new Point3D { X = edgeLength, Y = 0, Z = 0 },
										new Point3D { X = edgeLength, Y = 0, Z = edgeLength }
									}
								},
							}
						}
					};

					_cubeViewModel = new CubeViewModel(_algorithm.Cube);

					_xAngle = 0;
					_yAngle = 0;
					_zAngle = 0;

					XAngle = 0;
					YAngle = 0;
					ZAngle = 0;

					CubePositions = _cubeViewModel.Positions;
					TriangleIndices = _cubeViewModel.TriangleIndices;

					RecalculateCavalierProjection();

					PropertyChanged += ThreeDimentionalProjectionsViewModel_PropertyChanged;
				}));
			}
		}

		private void RecalculateCavalierProjection()
		{
			List<LineSegment> cubeProjectionEdges = _algorithm.CalculateCavalierProjection().ToList();

			foreach (LineSegment edge in cubeProjectionEdges)
			{
				edge.Start.X += ProjectionOffset;
				edge.Start.Y += ProjectionOffset;

				edge.End.X += ProjectionOffset;
				edge.End.Y += ProjectionOffset;
			}

			ProjectionLineSegments = new ObservableCollection<LineSegmentViewModel>(
				cubeProjectionEdges.Select(l => new LineSegmentViewModel(l)).ToList());
		}

		private void RotateCube(double xAngle, double yAngle, double zAngle)
		{
			_cubeViewModel.Rotate(xAngle, yAngle, zAngle);

			CubePositions = _cubeViewModel.Positions;
		}

		private void ThreeDimentionalProjectionsViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			if (e.PropertyName == nameof(CubePositions))
			{
				RecalculateCavalierProjection();
			}
		}
	}
}
