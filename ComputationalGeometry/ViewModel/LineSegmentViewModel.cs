﻿using ComputationalGeometry.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComputationalGeometry.ViewModel
{
	class LineSegmentViewModel
	{
		private readonly LineSegment _lineSegment;

		public LineSegmentViewModel(LineSegment lineSegment)
		{
			_lineSegment = lineSegment;
		}

		public double X1 => _lineSegment.Start.X;
		public double Y1 => _lineSegment.Start.Y;
		public double X2 => _lineSegment.End.X;
		public double Y2 => _lineSegment.End.Y;
	}
}
