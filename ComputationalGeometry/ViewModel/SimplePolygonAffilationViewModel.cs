﻿using ComputationalGeometry.Algorithm;
using ComputationalGeometry.Command;
using ComputationalGeometry.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using System.Windows.Media;

namespace ComputationalGeometry.ViewModel
{
	class SimplePolygonAffilationViewModel : PageViewModel
	{
		private readonly SimplePolygonAffilationAlgorithm _algorithm = new SimplePolygonAffilationAlgorithm();
		private readonly Polygon _polygon = new Polygon { Vertices = new List<Point>() };
		private ICommand _clearCanvasCommand;
		private Point _mousePosition = new Point();
		private SolidColorBrush _polygonColor = new SolidColorBrush();

		public override string Name => "Polygon Affilation";
		public int Height { get; set; } = 800;
		public int Width { get; set; } = 1000;
		public string PolygonPoints
		{
			get
			{
				return string.Join(" ", _polygon.Vertices.Select(p => $"{p.X}, {p.Y}"));
			}
			set
			{
				List<Point> vertices = new List<Point>();
				foreach (string pointString in value.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries))
				{
					string[] xy = pointString.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

					vertices.Add(new Point { X = double.Parse(xy[0]), Y = double.Parse(xy[1]) });
				}

				_polygon.Vertices = vertices;

				OnPropertyChanged(nameof(PolygonPoints));
			}
		}
		public Point MousePosition
		{
			get
			{
				return _mousePosition;
			}
			set
			{
				_mousePosition = value;

				CalculatePolygonAffilation();
				OnPropertyChanged(nameof(MousePosition));
			}
		}
		public SolidColorBrush PolygonColor
		{
			get
			{
				return _polygonColor;
			}
			set
			{
				_polygonColor = value;

				OnPropertyChanged(nameof(PolygonColor));
			}
		}

		public ICommand ClearCanvasCommand
		{
			get
			{
				return _clearCanvasCommand ?? (_clearCanvasCommand = new RelayCommand(obj =>
				{
					PolygonPoints = string.Empty;
				}));
			}
		}

		private void CalculatePolygonAffilation()
		{
			_algorithm.Polygon = _polygon;

			PolygonColor = new SolidColorBrush(_algorithm.DoesContain(MousePosition) ? Colors.LightPink : Colors.Transparent);
		}
	}
}
