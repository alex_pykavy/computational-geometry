﻿using ComputationalGeometry.Geometry;
using System.Collections.Generic;

namespace ComputationalGeometry.ViewModel
{
	class CubeViewModel : BaseViewModel
	{
		private readonly Cube _cube;
		private string _positions, _triangleIndices;

		public CubeViewModel(Cube cube)
		{
			_cube = cube;
		}

		public string Positions
		{
			get
			{
				RecalculateProperties();

				return _positions;
			}
		}
		public string TriangleIndices
		{
			get
			{
				RecalculateProperties();

				return _triangleIndices;
			}
		}

		public void Rotate(double xAngle, double yAngle, double zAngle)
		{
			_cube.Rotate(xAngle, yAngle, zAngle);
		}

		private void RecalculateProperties()
		{
			List<string> distinctPoints = new List<string>();
			List<string> triangleIndices = new List<string>();

			foreach (TriangleFacet facet in _cube.TriangleFacets)
			{
				List<int> trianlgeVertices = new List<int>();
				foreach (Point3D point in facet.Points)
				{
					string stringPoint = string.Format("{0} {1} {2}", point.X, point.Y, point.Z);

					int indexOfPoint = distinctPoints.IndexOf(stringPoint);
					if (indexOfPoint < 0)
					{
						indexOfPoint = distinctPoints.Count;

						distinctPoints.Add(stringPoint);
					}

					trianlgeVertices.Add(indexOfPoint);
				}

				triangleIndices.Add(string.Join(" ", trianlgeVertices));
			}

			_positions = string.Join("  ", distinctPoints);
			_triangleIndices = string.Join("  ", triangleIndices);
		}
	}
}
