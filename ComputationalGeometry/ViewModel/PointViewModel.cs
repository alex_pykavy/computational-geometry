﻿using ComputationalGeometry.Geometry;

namespace ComputationalGeometry.ViewModel
{
	class PointViewModel : BaseViewModel
	{
		private readonly Point _point;

		public PointViewModel(Point point)
		{
			_point = point;
		}

		public double Diameter => 4;
		public double X
		{
			get
			{
				return _point.X - Diameter / 2;
			}
			set
			{
				_point.X = value;

				OnPropertyChanged(nameof(X));
			}
		}

		public double Y
		{
			get
			{
				return _point.Y - Diameter / 2;
			}
			set
			{
				_point.Y = value;

				OnPropertyChanged(nameof(Y));
			}
		}
	}
}
