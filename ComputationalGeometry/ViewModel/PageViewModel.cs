﻿namespace ComputationalGeometry.ViewModel
{
	abstract class PageViewModel : BaseViewModel
	{
		public abstract string Name { get; }
	}
}
