﻿using ComputationalGeometry.Command;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;

namespace ComputationalGeometry.ViewModel
{
	class ApplicationViewModel : BaseViewModel
	{
		private ICommand _changePageCommand;
		private PageViewModel _currentPageViewModel;

		public PageViewModel CurrentPageViewModel
		{
			get
			{
				if (_currentPageViewModel == null)
				{
					CurrentPageViewModel = PageViewModels[0];
				}

				return _currentPageViewModel;
			}
			set
			{
				_currentPageViewModel = value;

				OnPropertyChanged(nameof(CurrentPageViewModel));
			}
		}
		public List<PageViewModel> PageViewModels { get; } = new List<PageViewModel>
		{
			new LocalizationViewModel(),
			new IntersectionViewModel(),
			new ConvexHullViewModel(),
			new VoronoiDiagramViewModel(),
			new ThreeDimentionalProjectionsViewModel(),
			new HermiteCurveViewModel(),
			new SimplePolygonAffilationViewModel()
		};

		public ICommand ChangePageCommand => _changePageCommand ?? (_changePageCommand = new RelayCommand(
			p => ChangePageViewModel(p as PageViewModel),
			p => p is PageViewModel));

		private void ChangePageViewModel(PageViewModel pageViewModel)
		{
			CurrentPageViewModel = PageViewModels.FirstOrDefault(p => p.Name == pageViewModel.Name);
		}
	}
}
