﻿using System;
using System.Windows.Input;

namespace ComputationalGeometry.Command
{
	public class RelayCommand : ICommand
	{
		private readonly Action<object> _executeAction;
		private readonly Predicate<object> _canExecutePredicate;

		public event EventHandler CanExecuteChanged
		{
			add { CommandManager.RequerySuggested += value; }
			remove { CommandManager.RequerySuggested -= value; }
		}

		public RelayCommand(Action<object> executeAction, Predicate<object> canExecutePredicate = null)
		{
			_executeAction = executeAction;
			_canExecutePredicate = canExecutePredicate;
		}

		public bool CanExecute(object parameter)
		{
			return _canExecutePredicate == null || _canExecutePredicate(parameter);
		}

		public void Execute(object parameter)
		{
			_executeAction(parameter);
		}
	}
}
