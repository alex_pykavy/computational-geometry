﻿namespace ComputationalGeometry.Mathematics
{
	static class DoubleExtensions
	{
		public static bool IsBetween(this double p, double left, double right)
		{
			return p >= left && p <= right;
		}
	}
}
