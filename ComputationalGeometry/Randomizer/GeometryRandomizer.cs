﻿using ComputationalGeometry.Geometry;
using System;
using System.Collections.Generic;

namespace ComputationalGeometry.Randomizer
{
	public class GeometryRandomizer
	{
		private readonly Random _random = new Random();

		private readonly double _windowHeight;
		private readonly double _windowWidth;

		public GeometryRandomizer(double windowHeight, double windowWidth)
		{
			_windowHeight = windowHeight;
			_windowWidth = windowWidth;
		}

		public Point NextPoint()
		{
			return new Point
			{
				X = _random.NextDouble() * _windowWidth,
				Y = _random.NextDouble() * _windowHeight
			};
		}

		public Point NextPoint(double minX, double maxX, double minY, double maxY)
		{
			return new Point
			{
				X = minX + _random.NextDouble() * (maxX - minX),
				Y = minY + _random.NextDouble() * (maxY - minY)
			};
		}

		public IEnumerable<Point> NextPoints(int amount)
		{
			for (int i = 0; i < amount; i++)
			{
				yield return NextPoint();
			}
		}

		public IEnumerable<Point> NextPoints(int amount, double minX, double maxX, double minY, double maxY)
		{
			for (int i = 0; i < amount; i++)
			{
				yield return NextPoint(minX, maxX, minY, maxY);
			}
		}

		public LineSegment NextLineSegment(double minLength, double maxLength)
		{
			double length = minLength + _random.NextDouble() * (maxLength - minLength);
			double angle = Math.PI * _random.NextDouble() - Math.PI / 2;

			Point startPoint = NextPoint(length, _windowWidth - length, length, _windowHeight - length);
			Point endPoint = new Point { X = startPoint.X + length * Math.Cos(angle), Y = startPoint.Y + length * Math.Sin(angle) };

			return new LineSegment
			{
				Start = startPoint,
				End = endPoint
			};
		}

		public IEnumerable<LineSegment> NextLineSegments(int amount, double minLength, double maxLength)
		{
			for (int i = 0; i < amount; i++)
			{
				yield return NextLineSegment(minLength, maxLength);
			}
		}
	}
}
