﻿using ComputationalGeometry.Geometry;
using ComputationalGeometry.ViewModel;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Shapes;

namespace ComputationalGeometry.View
{
	/// <summary>
	/// Interaction logic for SimplePolygonAffilationView.xaml
	/// </summary>
	public partial class SimplePolygonAffilationView : UserControl
	{
		private readonly Canvas _canvas;
		private readonly System.Windows.Shapes.Polygon _polygon;

		public SimplePolygonAffilationView()
		{
			InitializeComponent();

			_polygon = SimplePolygon;
			_canvas = _polygon.Parent as Canvas;
		}

		private void Canvas_MouseUp(object sender, MouseButtonEventArgs e)
		{
			var mousePosition = e.GetPosition(_canvas);

			SimplePolygonAffilationViewModel viewModel = DataContext as SimplePolygonAffilationViewModel;

			// just to get points in sting for viewModel afterwards
			_polygon.Points.Add(mousePosition);

			viewModel.PolygonPoints = _polygon.Points.ToString();
		}

		private void Canvas_MouseMove(object sender, MouseEventArgs e)
		{
			var mousePosition = e.GetPosition(_canvas);

			SimplePolygonAffilationViewModel viewModel = DataContext as SimplePolygonAffilationViewModel;

			viewModel.MousePosition = new Point { X = mousePosition.X, Y = mousePosition.Y };
		}
	}
}
