﻿using ComputationalGeometry.CodeBehind;
using System.Windows.Controls;
using System.Windows.Input;

namespace ComputationalGeometry.View
{
	/// <summary>
	/// Interaction logic for LocalizationView.xaml
	/// </summary>
	public partial class LocalizationView : UserControl
	{
		private readonly RectangleDragAndDrop _rectangleDragAndDrop;

		public LocalizationView()
		{
			InitializeComponent();

			_rectangleDragAndDrop = new RectangleDragAndDrop(SelectionRectangle);
		}

		private void Canvas_MouseDown(object sender, MouseButtonEventArgs e)
		{
			_rectangleDragAndDrop.Canvas_MouseDown(sender, e);
		}

		private void Canvas_MouseMove(object sender, MouseEventArgs e)
		{
			_rectangleDragAndDrop.Canvas_MouseMove(sender, e);
		}

		private void Canvas_MouseUp(object sender, MouseButtonEventArgs e)
		{
			_rectangleDragAndDrop.Canvas_MouseUp(sender, e);
		}

		private void Rect_MouseDown(object sender, MouseButtonEventArgs e)
		{
			_rectangleDragAndDrop.Rect_MouseDown(sender, e);
		}
	}
}
