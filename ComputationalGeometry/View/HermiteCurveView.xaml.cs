﻿using ComputationalGeometry.CodeBehind;
using System.Windows.Controls;
using System.Windows.Input;

namespace ComputationalGeometry.View
{
	/// <summary>
	/// Interaction logic for HermiteCurveView.xaml
	/// </summary>
	public partial class HermiteCurveView : UserControl
	{
		private readonly PointPaint _pointPaint;

		public HermiteCurveView()
		{
			InitializeComponent();

			_pointPaint = new PointPaint(ControlPoints);
		}

		private void Canvas_MouseUp(object sender, MouseButtonEventArgs e)
		{
			_pointPaint.Canvas_MouseUp(sender, e);
		}

		private void Canvas_MouseMove(object sender, MouseEventArgs e)
		{
			_pointPaint.Canvas_MouseMove(sender, e);
		}

		private void Ellipse_MouseDown(object sender, MouseButtonEventArgs e)
		{
			_pointPaint.PointEllipse_MouseDown(sender, e);
		}
	}
}
