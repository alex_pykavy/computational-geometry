﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Shapes;

namespace ComputationalGeometry.CodeBehind
{
	class RectangleDragAndDrop
	{
		private readonly Rectangle _rectangle;
		private readonly Canvas _canvas;
		private Point? _canvasDownPoint = null;
		private Point? _rectangleDownPoint = null;

		public RectangleDragAndDrop(Rectangle rectangle)
		{
			_rectangle = rectangle;
			_canvas = rectangle.Parent as Canvas;
		}

		public void Canvas_MouseDown(object sender, MouseButtonEventArgs e)
		{
			Point mousePosition = e.GetPosition(_canvas);

			if (!_rectangleDownPoint.HasValue)
			{
				_canvasDownPoint = mousePosition;

				Canvas.SetLeft(_rectangle, mousePosition.X);
				Canvas.SetTop(_rectangle, mousePosition.Y);
			}
		}

		public void Canvas_MouseMove(object sender, MouseEventArgs e)
		{
			Point mousePosition = e.GetPosition(_canvas);

			if (_rectangleDownPoint.HasValue)
			{
				Canvas.SetLeft(_rectangle, mousePosition.X);
				Canvas.SetTop(_rectangle, mousePosition.Y);
			}
			else if (_canvasDownPoint.HasValue)
			{
				double width = mousePosition.X - _canvasDownPoint.Value.X;
				double height = mousePosition.Y - _canvasDownPoint.Value.Y;

				if (width < 0)
				{
					Canvas.SetLeft(_rectangle, mousePosition.X);
				}

				if (height < 0)
				{
					Canvas.SetTop(_rectangle, mousePosition.Y);
				}

				_rectangle.Width = Math.Abs(width);
				_rectangle.Height = Math.Abs(height);
			}
			else
			{
			}
		}

		public void Canvas_MouseUp(object sender, MouseButtonEventArgs e)
		{
			_canvasDownPoint = null;
			_rectangleDownPoint = null;

			if (_rectangle.Width < 10 && _rectangle.Height < 10)
			{
				MessageBox.Show("Too small rectangle");
			}
		}

		public void Rect_MouseDown(object sender, MouseButtonEventArgs e)
		{
			_rectangleDownPoint = e.GetPosition(_canvas);
		}
	}
}
