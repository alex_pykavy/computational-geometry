﻿using ComputationalGeometry.Geometry;
using ComputationalGeometry.ViewModel;
using System.Collections.ObjectModel;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Shapes;

namespace ComputationalGeometry.CodeBehind
{
	class PointPaint
	{
		private readonly ItemsControl _controlPointsItemsControl;
		private readonly Canvas _canvas;
		private PointViewModel _capturedPointViewModel = null;

		public PointPaint(ItemsControl controlPoints)
		{
			_controlPointsItemsControl = controlPoints;
			_canvas = controlPoints.Parent as Canvas;
		}

		public void Canvas_MouseUp(object sender, MouseButtonEventArgs e)
		{
			var mousePosition = e.GetPosition(_canvas);

			if (_capturedPointViewModel == null)
			{
				ObservableCollection<PointViewModel> extendedControlPoints
					= _controlPointsItemsControl.ItemsSource as ObservableCollection<PointViewModel>;
				extendedControlPoints.Add(new PointViewModel(new Point { X = mousePosition.X, Y = mousePosition.Y }));
			}
			else
			{
				_capturedPointViewModel = null;
			}
		}

		public void Canvas_MouseMove(object sender, MouseEventArgs e)
		{
			var mousePosition = e.GetPosition(_canvas);
			if (_capturedPointViewModel != null)
			{
				ObservableCollection<PointViewModel> extendedControlPoints
					= _controlPointsItemsControl.ItemsSource as ObservableCollection<PointViewModel>;

				_capturedPointViewModel.X = mousePosition.X;
				_capturedPointViewModel.Y = mousePosition.Y;

				// to raise CollectionChanged
				extendedControlPoints.Move(0, 0);
			}
		}

		public void PointEllipse_MouseDown(object sender, MouseButtonEventArgs e)
		{
			Ellipse ellipse = sender as Ellipse;
			_capturedPointViewModel = ellipse.DataContext as PointViewModel;
		}
	}
}
