﻿using ComputationalGeometry.Geometry;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ComputationalGeometry.DataStructure
{
	class QuadTreeNode
	{
		private readonly int _maxNodeCapacity;
		private readonly Rectangle _boundary;
		private readonly Collection<Point> _points = new Collection<Point>();

		private QuadTreeNode _northWest;
		private QuadTreeNode _northEast;
		private QuadTreeNode _southWest;
		private QuadTreeNode _southEast;

		public QuadTreeNode(int maxNodeCapacity, double maxX, double maxY)
			: this(maxNodeCapacity, 0, maxX, 0, maxY)
		{
		}

		public QuadTreeNode(int maxNodeCapacity, double minX, double maxX, double minY, double maxY)
		{
			_maxNodeCapacity = maxNodeCapacity;
			_boundary = new Rectangle
			{
				X1 = minX,
				Y1 = minY,
				X2 = maxX,
				Y2 = maxY
			};
		}

		public bool Insert(Point p)
		{
			if (!_boundary.Contains(p))
			{
				return false;
			}

			if (_points.Count < _maxNodeCapacity)
			{
				_points.Add(p);

				return true;
			}

			if (_northWest == null)
			{
				SubDivide();
			}

			return _northWest.Insert(p) || _northEast.Insert(p) || _southWest.Insert(p) || _southEast.Insert(p);
		}

		public IEnumerable<Point> SelectPoints(Rectangle region)
		{
			List<Point> selectedPoints = new List<Point>();

			if (!_boundary.Intersects(region))
			{
				return selectedPoints;
			}

			foreach (Point p in _points)
			{
				if (region.Contains(p))
				{
					selectedPoints.Add(p);
				}
			}

			if (_northWest != null)
			{
				selectedPoints.AddRange(_northWest.SelectPoints(region));
				selectedPoints.AddRange(_northEast.SelectPoints(region));
				selectedPoints.AddRange(_southWest.SelectPoints(region));
				selectedPoints.AddRange(_southEast.SelectPoints(region));
			}

			return selectedPoints;
		}

		private void SubDivide()
		{
			double boundaryCenterX = _boundary.X1 + (_boundary.X2 - _boundary.X1) / 2;
			double boundaryCenterY = _boundary.Y1 + (_boundary.Y2 - _boundary.Y1) / 2;

			_northWest = new QuadTreeNode(_maxNodeCapacity, _boundary.X1, boundaryCenterX, _boundary.Y1, boundaryCenterY);
			_northEast = new QuadTreeNode(_maxNodeCapacity, boundaryCenterX, _boundary.X2, _boundary.Y1, boundaryCenterY);
			_southWest = new QuadTreeNode(_maxNodeCapacity, _boundary.X1, boundaryCenterX, boundaryCenterY, _boundary.Y2);
			_southEast = new QuadTreeNode(_maxNodeCapacity, boundaryCenterX, _boundary.X2, boundaryCenterY, _boundary.Y2);
		}
	}
}
