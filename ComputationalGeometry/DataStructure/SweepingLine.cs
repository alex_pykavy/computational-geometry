﻿using System.Collections.Generic;

namespace ComputationalGeometry.DataStructure
{
	class SweepingLine<T>
	{
		private readonly SortedList<T, T> _sweepingLineStatus;

		public int Count => _sweepingLineStatus.Count;

		public IComparer<T> Comparer => _sweepingLineStatus.Comparer;

		public SweepingLine(IComparer<T> comparer)
		{
			_sweepingLineStatus = new SortedList<T, T>(comparer);
		}

		public void Add(T value)
		{
			try
			{
				_sweepingLineStatus.Add(value, value);
			}
			catch
			{
			}
		}

		public void Remove(T value)
		{
			_sweepingLineStatus.Remove(value);
		}

		public T GetAbove(T value)
		{
			int valueIndex = _sweepingLineStatus.IndexOfValue(value);
			if (valueIndex < 0 || valueIndex == _sweepingLineStatus.Count - 1)
			{
				return default(T);
			}

			return _sweepingLineStatus[_sweepingLineStatus.Keys[valueIndex + 1]];
		}

		public T GetBelow(T value)
		{
			int valueIndex = _sweepingLineStatus.IndexOfValue(value);
			if (valueIndex < 1)
			{
				return default(T);
			}

			return _sweepingLineStatus[_sweepingLineStatus.Keys[valueIndex - 1]];
		}

		public IEnumerator<T> GetEnumerator()
		{
			foreach (KeyValuePair<T, T> pair in _sweepingLineStatus)
			{
				yield return pair.Value;
			}
		}
	}
}
