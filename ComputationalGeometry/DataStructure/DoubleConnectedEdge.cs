﻿using ComputationalGeometry.Geometry;

namespace ComputationalGeometry.DataStructure
{
	class DoubleConnectedEdge
	{
		public Point Point1 { get; set; }
		public Point Point2 { get; set; }
		public LineSegment Edge1 { get; set; }
		public LineSegment Edge2 { get; set; }
		public Point Face1 { get; set; }
		public Point Face2 { get; set; }
	}
}
