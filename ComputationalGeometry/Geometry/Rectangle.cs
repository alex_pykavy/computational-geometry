﻿using ComputationalGeometry.Mathematics;

namespace ComputationalGeometry.Geometry
{
	public class Rectangle
	{
		public double X1 { get; set; }
		public double Y1 { get; set; }
		public double X2 { get; set; }
		public double Y2 { get; set; }

		public bool Contains(Point p)
		{
			return p.X.IsBetween(X1, X2) && p.Y.IsBetween(Y1, Y2);
		}

		public bool Intersects(Rectangle r)
		{
			if (X1 > r.X2 || X2 < r.X1 || Y1 > r.Y2 || Y2 < r.Y1)
			{
				return false;
			}

			return true;
		}
	}
}
