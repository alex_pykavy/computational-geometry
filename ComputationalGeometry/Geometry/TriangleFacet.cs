﻿namespace ComputationalGeometry.Geometry
{
	public class TriangleFacet
	{
		public Point3D[] Points { get; set; }
	}
}
