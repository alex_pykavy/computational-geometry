﻿using System;

namespace ComputationalGeometry.Geometry
{
	public class LineSegment3D
	{
		public Point3D Start { get; set; }
		public Point3D End { get; set; }

		public double Length =>  Math.Sqrt(Math.Pow((Start.X - End.X), 2) + Math.Pow((Start.Y - End.Y), 2) + Math.Pow((Start.Z - End.Z), 2));
	}
}
