﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Media.Media3D;

namespace ComputationalGeometry.Geometry
{
	public class Cube
	{
		public TriangleFacet[] TriangleFacets { get; set; }

		public IEnumerable<LineSegment3D> Edges
		{
			get
			{
				// Bug: it will contain duplicates :(
				HashSet<LineSegment3D> cubeEdges = new HashSet<LineSegment3D>();

				foreach (TriangleFacet facet in TriangleFacets)
				{
					LineSegment3D[] facetEdges = new LineSegment3D[]
					{
						new LineSegment3D
						{
							Start = facet.Points[0],
							End = facet.Points[1]
						},
						new LineSegment3D
						{
							Start = facet.Points[1],
							End = facet.Points[2]
						},
						new LineSegment3D
						{
							Start = facet.Points[2],
							End = facet.Points[0]
						}
					};

					foreach (LineSegment3D edge in facetEdges.OrderBy(e => e.Length).Take(2))
					{
						cubeEdges.Add(edge);
					}
				}

				return cubeEdges;
			}
		}

		public void Rotate(double xAngle, double yAngle, double zAngle)
		{
			Matrix3D rotationMatrix = new Matrix3D();
			System.Windows.Media.Media3D.Point3D cubeCenter = new System.Windows.Media.Media3D.Point3D { X = 25, Y = 25, Z = 25 };

			rotationMatrix.RotateAt(new Quaternion(new Vector3D(1, 0, 0), xAngle), cubeCenter);
			rotationMatrix.RotateAt(new Quaternion(new Vector3D(0, 1, 0) * rotationMatrix, yAngle), cubeCenter);
			rotationMatrix.RotateAt(new Quaternion(new Vector3D(0, 0, 1) * rotationMatrix, zAngle), cubeCenter);

			foreach (TriangleFacet facet in TriangleFacets)
			{
				foreach (Point3D point in facet.Points)
				{
					System.Windows.Media.Media3D.Point3D standardPoint3D = new System.Windows.Media.Media3D.Point3D
					{
						X = point.X,
						Y = point.Y,
						Z = point.Z
					};

					System.Windows.Media.Media3D.Point3D rotatedStandardPoint3D = standardPoint3D * rotationMatrix;

					point.X = rotatedStandardPoint3D.X;
					point.Y = rotatedStandardPoint3D.Y;
					point.Z = rotatedStandardPoint3D.Z;
				}
			}
		}
	}
}
