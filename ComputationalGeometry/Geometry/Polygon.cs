﻿using System.Collections.Generic;

namespace ComputationalGeometry.Geometry
{
	class Polygon
	{
		public List<Point> Vertices { get; set; }
	}
}
