﻿using System;

namespace ComputationalGeometry.Geometry
{
	public class Point
	{
		public double X { get; set; }
		public double Y { get; set; }

		public static Point operator +(Point a, Point b)
		{
			return new Point { X = a.X + b.X, Y = a.Y + b.Y };
		}

		public static Point operator -(Point a, Point b)
		{
			return new Point { X = a.X - b.X, Y = a.Y - b.Y };
		}

		public static Point operator *(double scalar, Point a)
		{
			return new Point { X = scalar * a.X, Y = scalar * a.Y };
		}
	}
}
