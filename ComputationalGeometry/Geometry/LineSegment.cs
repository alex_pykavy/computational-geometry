﻿using ComputationalGeometry.Mathematics;

namespace ComputationalGeometry.Geometry
{
	public class LineSegment
	{
		public Point Start { get; set; }
		public Point End { get; set; }

		public Point FindIntersectionPoint(LineSegment l)
		{
			// Assume that direction vectors for line segments are (m1, p1) and (m2, p2)
			double m1 = End.X - Start.X, p1 = End.Y - Start.Y;
			double m2 = l.End.X - l.Start.X, p2 = l.End.Y - l.Start.Y;

			double s = (m1 * (l.Start.Y - Start.Y) - p1 * (l.Start.X- Start.X)) / (p1 * m2 - p2 * m1);
			double t = (l.Start.X - Start.X + m2 * s) / m1;

			if (s.IsBetween(0, 1) && t.IsBetween(0, 1))
			{
				return new Point { X = Start.X + m1 * t, Y = Start.Y + p1 * t };
			}

			return null;
		}
	}
}
