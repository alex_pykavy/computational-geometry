﻿using ComputationalGeometry.Geometry;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace ComputationalGeometry.ShewchukPredicates
{
	public static class ShewchukPredicatesWrapper
	{
		static ShewchukPredicatesWrapper()
		{
			Init();
		}

		public static IComparer<Point> CreateGrahamComparer(Point minPoint)
		{
			return Comparer<Point>.Create((a, b) => orient2d(PointToDoubleArray(minPoint), PointToDoubleArray(b), PointToDoubleArray(a)));
		}

		public static bool DoLineSegmentsIntersect(LineSegment a, LineSegment b)
		{
			return orient2d(PointToDoubleArray(a.Start), PointToDoubleArray(a.End), PointToDoubleArray(b.Start)) !=
				orient2d(PointToDoubleArray(a.Start), PointToDoubleArray(a.End), PointToDoubleArray(b.End)) &&
				orient2d(PointToDoubleArray(b.Start), PointToDoubleArray(b.End), PointToDoubleArray(a.Start)) !=
				orient2d(PointToDoubleArray(b.Start), PointToDoubleArray(b.End), PointToDoubleArray(a.End));
		}

		public static bool IsLeftTurn(Point a, Point b, Point c)
		{
			return orient2d(PointToDoubleArray(a), PointToDoubleArray(b), PointToDoubleArray(c)) > 0;
		}

		private static double[] PointToDoubleArray(Point p)
		{
			return new double[] { p.X, p.Y };
		}

		[DllImport("ShewchukPredicates.dll", CallingConvention = CallingConvention.Cdecl)]
		private static extern void Init();

		[DllImport("ShewchukPredicates.dll", CallingConvention = CallingConvention.Cdecl)]
		private static extern int orient2d(double[] pa, double[] pb, double[] pc);

		[DllImport("ShewchukPredicates.dll", CallingConvention = CallingConvention.Cdecl)]
		private static extern int incircle(double[] pa, double[] pb, double[] pc, double[] pd);
	}
}
