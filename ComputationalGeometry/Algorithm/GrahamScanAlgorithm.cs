﻿using System;
using System.Collections.Generic;
using ComputationalGeometry.Geometry;
using System.Linq;
using ComputationalGeometry.ShewchukPredicates;

namespace ComputationalGeometry.Algorithm
{
	public class GrahamScanAlgorithm : ConvexHullAlgorithm
	{
		public GrahamScanAlgorithm(double height, double width) : base(height, width)
		{
		}

		public override IEnumerable<Point> Run()
		{
			List<Point> sortedPoints = ObtainPointsOrderedByPolarAngle();

			Stack<Point> convexHullPoints = new Stack<Point>();
			convexHullPoints.Push(sortedPoints.First());
			convexHullPoints.Push(sortedPoints.Skip(1).First());

			for (int i = 2; i < sortedPoints.Count; i++)
			{
				while (!IsLeftTurn(convexHullPoints, sortedPoints[i]))
				{
					convexHullPoints.Pop();
				}

				convexHullPoints.Push(sortedPoints[i]);
			}

			return convexHullPoints.Reverse();
		}

		private Point FindMinPoint()
		{
			Point minPoint = Points.FirstOrDefault();
			foreach (Point p in Points)
			{
				if (p.Y < minPoint.Y || p.Y == minPoint.Y && p.X < minPoint.X)
				{
					minPoint = p;
				}
			}

			return minPoint;
		}

		private bool IsLeftTurn(Stack<Point> stack, Point currentPoint)
		{
			Point topPoint = stack.First();
			Point nextToTopPoint = stack.Skip(1).First();
			Point a = nextToTopPoint, b = topPoint, c = currentPoint;

			return ShewchukPredicatesWrapper.IsLeftTurn(a, b, c);
		}

		private List<Point> ObtainPointsOrderedByPolarAngle()
		{
			Point minPoint = FindMinPoint();

			List<Point> minPointList = new List<Point> { minPoint };
			return minPointList.Concat(
				Points.Except(minPointList).OrderBy(p => p, ShewchukPredicatesWrapper.CreateGrahamComparer(minPoint))).ToList();

		}
	}
}
