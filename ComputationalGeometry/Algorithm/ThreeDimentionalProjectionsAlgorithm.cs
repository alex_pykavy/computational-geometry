﻿using ComputationalGeometry.Geometry;
using System;
using System.Collections;
using System.Collections.Generic;

namespace ComputationalGeometry.Algorithm
{
	class ThreeDimentionalProjectionsAlgorithm
	{
		public double Alpha => Math.PI / 4;
		public double Phi => Math.PI / 6;
		public Cube Cube { get; set; }

		public IEnumerable<LineSegment> CalculateCavalierProjection()
		{
			foreach (LineSegment3D edge in Cube.Edges)
			{
				yield return CalculateCavalierProjection(edge);
			}
		}

		private LineSegment CalculateCavalierProjection(LineSegment3D lineSegment3D)
		{
			return new LineSegment
			{
				Start = CalculateCavalierProjection(lineSegment3D.Start),
				End = CalculateCavalierProjection(lineSegment3D.End)
			};
		}

		private Point CalculateCavalierProjection(Point3D point3D)
		{
			double l = Alpha == 0 ? 0 : point3D.Z / Math.Tan(Alpha);

			return new Point
			{
				X = point3D.X + l * Math.Cos(Phi),
				Y = point3D.Y + l * Math.Sign(Phi)
			};
		}
	}
}
