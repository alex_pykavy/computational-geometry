﻿using ComputationalGeometry.Geometry;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ComputationalGeometry.Algorithm
{
	public abstract class ConvexHullAlgorithm
	{
		public double Height { get; }
		public double Width { get; }
		public Collection<Point> Points { get; set; }

		public ConvexHullAlgorithm(double height, double width)
		{
			Height = height;
			Width = width;
		}

		public abstract IEnumerable<Point> Run();
	}
}
