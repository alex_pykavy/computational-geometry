﻿using ComputationalGeometry.Geometry;
using System;
using System.Collections.Generic;

namespace ComputationalGeometry.Algorithm
{
	class HermiteCurveAlgorithm
	{
		private static readonly Func<double, double>[] BezierPolynoms = new Func<double, double>[]
		{
			t => Math.Pow(1 - t, 3),
			t => 3 * t * Math.Pow(1 - t, 2),
			t => 3 * Math.Pow(t, 2) * (1 - t),
			t => Math.Pow(t, 3)
		};

		private static readonly Func<double, double>[] HermitePolynoms = new Func<double, double>[]
		{
			t => BezierPolynoms[0](t) + BezierPolynoms[1](t),
			t => BezierPolynoms[1](t) / 3,
			t => -BezierPolynoms[2](t) / 3,
			t => BezierPolynoms[2](t) + BezierPolynoms[3](t),
		};

		public int CurvePointsAmount { get; set; } = 200;

		public IEnumerable<Point> Run(List<Point> controlPoints)
		{
			Point p0 = controlPoints[0];
			Point m0 = 3 * (controlPoints[1] - controlPoints[0]);
			Point m1 = 3 * (controlPoints[3] - controlPoints[2]);
			Point p1 = controlPoints[3];

			double t = 0, step = 1.0 / CurvePointsAmount;
			for (int i = 0; i < CurvePointsAmount; i++)
			{
				t += step;

				yield return HermitePolynoms[0](t) * p0
							+ HermitePolynoms[1](t) * m0
							+ HermitePolynoms[2](t) * m1
							+ HermitePolynoms[3](t) * p1;
			}
		}
	}
}
