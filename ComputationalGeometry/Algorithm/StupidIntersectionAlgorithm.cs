﻿using ComputationalGeometry.Geometry;
using System.Collections.Generic;

namespace ComputationalGeometry.Algorithm
{
	public class StupidIntersectionAlgorithm : IntersectionAlgorithm
	{
		public override IEnumerable<Point> Run()
		{
			foreach (LineSegment first in LineSegments)
			{
				foreach (LineSegment second in LineSegments)
				{
					Point intersectionPoint = first.FindIntersectionPoint(second);
					if (intersectionPoint != null)
					{
						yield return intersectionPoint;
					}
				}
			}
		}
	}
}
