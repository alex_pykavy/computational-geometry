﻿using ComputationalGeometry.Geometry;
using System.Collections.Generic;

namespace ComputationalGeometry.Algorithm
{
	public class StupidLocalizationAlgorithm : LocalizationAlgorithm
	{
		public StupidLocalizationAlgorithm(double height, double width) : base(height, width)
		{
		}

		public override void Prepare()
		{
		}

		public override IEnumerable<Point> Run()
		{
			foreach (Point p in Points)
			{
				if (Region.Contains(p))
				{
					yield return p;
				}
			}
		}
	}
}
