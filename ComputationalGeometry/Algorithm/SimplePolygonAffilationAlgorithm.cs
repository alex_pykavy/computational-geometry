﻿using ComputationalGeometry.Geometry;
using System.Collections.Generic;
using System.Linq;
using System;

namespace ComputationalGeometry.Algorithm
{
	class SimplePolygonAffilationAlgorithm
	{
		public Polygon Polygon { get; set; }

		public bool DoesContain(Point p)
		{
			if (Polygon.Vertices.Count < 3)
			{
				return false;
			}

			double maxX = 0;
			foreach (Point polygonVertex in Polygon.Vertices)
			{
				if (polygonVertex.X > maxX)
				{
					maxX = polygonVertex.X;
				}
			}

			maxX += 10;

			LineSegment secantLineSegment = new LineSegment
			{
				Start = p,
				End = new Point { X = maxX, Y = p.Y }
			};

			SortedList<Point, LineSegment> intersectionPoints = new SortedList<Point, LineSegment>(
				Comparer<Point>.Create((a, b) =>
				{
					int xCompareResult = a.X.CompareTo(b.X);
					if (xCompareResult == 0)
					{
						return a.Y.CompareTo(b.Y);
					}

					return xCompareResult;
				}));
			Point firstPoint = Polygon.Vertices.First();
			foreach (Point secondPoint in Polygon.Vertices.Skip(1).Concat(Polygon.Vertices.Take(1)))
			{
				LineSegment polygonEdge = new LineSegment
				{
					Start = firstPoint,
					End = secondPoint
				};

				Point intersectionPoint = secantLineSegment.FindIntersectionPoint(polygonEdge);
				if (intersectionPoint != null)
				{
					if (intersectionPoints.ContainsKey(intersectionPoint))
					{
						LineSegment existingPolygonEdge = intersectionPoints[intersectionPoint];

						// check whether point is touching or crossing
						int existingIntersectionPointMagicNumber =
							existingPolygonEdge.Start.Y.CompareTo(p.Y) + existingPolygonEdge.End.Y.CompareTo(p.Y);
						int intersectionPointMagicNumber =
							polygonEdge.Start.Y.CompareTo(p.Y) + polygonEdge.End.Y.CompareTo(p.Y);

						if (existingIntersectionPointMagicNumber * intersectionPointMagicNumber >= 0)
						{
							intersectionPoints.Remove(intersectionPoint);
						}
					}
					else
					{
						intersectionPoints.Add(intersectionPoint, polygonEdge);
					}
				}

				firstPoint = secondPoint;
			}

			return intersectionPoints.Count % 2 == 1;
		}
	}
}
