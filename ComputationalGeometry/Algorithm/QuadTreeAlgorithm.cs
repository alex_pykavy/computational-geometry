﻿using ComputationalGeometry.DataStructure;
using ComputationalGeometry.Geometry;
using System.Collections.Generic;

namespace ComputationalGeometry.Algorithm
{
	public class QuadTreeAlgorithm : LocalizationAlgorithm
	{
		private readonly QuadTreeNode _quadTree;

		public QuadTreeAlgorithm(int maxNodeCapacity, double height, double width) : base(height, width)
		{
			_quadTree = new QuadTreeNode(maxNodeCapacity, Width, Height);
		}

		public override void Prepare()
		{
			foreach (Point p in Points)
			{
				_quadTree.Insert(p);
			}
		}

		public override IEnumerable<Point> Run()
		{
			foreach (Point p in _quadTree.SelectPoints(Region))
			{
				yield return p;
			}
		}
	}
}
