﻿using ComputationalGeometry.Geometry;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ComputationalGeometry.Algorithm
{
	public abstract class IntersectionAlgorithm
	{
		public Collection<LineSegment> LineSegments { get; set; }

		public abstract IEnumerable<Point> Run();
	}
}
