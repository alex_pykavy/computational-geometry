﻿using ComputationalGeometry.Geometry;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ComputationalGeometry.Algorithm
{
	public abstract class LocalizationAlgorithm
	{
		public double Height { get; }
		public double Width { get; }
		public Collection<Point> Points { get; set; }
		public Rectangle Region { get; set; }

		public LocalizationAlgorithm(double height, double width)
		{
			Height = height;
			Width = width;
		}

		public abstract void Prepare();
		public abstract IEnumerable<Point> Run();
	}
}
