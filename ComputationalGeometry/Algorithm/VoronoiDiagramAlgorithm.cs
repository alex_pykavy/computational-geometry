﻿using ComputationalGeometry.Geometry;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ComputationalGeometry.Algorithm
{
	public abstract class VoronoiDiagramAlgorithm
	{
		public double Height { get; }
		public double Width { get; }
		public Collection<Point> Points { get; set; }

		public VoronoiDiagramAlgorithm(double height, double width)
		{
			Height = height;
			Width = width;
		}

		public abstract IEnumerable<LineSegment> Run();
	}
}
