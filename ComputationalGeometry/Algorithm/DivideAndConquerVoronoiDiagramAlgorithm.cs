﻿using ComputationalGeometry.Geometry;
using System.Collections.Generic;

namespace ComputationalGeometry.Algorithm
{
	public class DivideAndConquerVoronoiDiagramAlgorithm : VoronoiDiagramAlgorithm
	{
		public DivideAndConquerVoronoiDiagramAlgorithm(double height, double width) : base(height, width)
		{
		}

		public override IEnumerable<LineSegment> Run()
		{
			yield return new LineSegment { Start = Points[0], End = Points[1] };
		}
	}
}
