﻿using System.Collections.Generic;
using ComputationalGeometry.Geometry;
using ComputationalGeometry.DataStructure;
using ComputationalGeometry.ShewchukPredicates;

namespace ComputationalGeometry.Algorithm
{
	public class BentleyOttmannAlgorithm : IntersectionAlgorithm
	{
		private readonly SortedSet<EventPoint> _eventPoints = new SortedSet<EventPoint>(new EventPointComparer());
		private readonly Queue<EventPoint> _intersectionEventPoints = new Queue<EventPoint>();
		private readonly SweepingLineComparer _sweepingLineComparer = new SweepingLineComparer();
		private readonly SweepingLine<LineSegment> _sweepingLineStatus;

		public BentleyOttmannAlgorithm()
		{
			_sweepingLineStatus = new SweepingLine<LineSegment>(_sweepingLineComparer);
		}

		public override IEnumerable<Point> Run()
		{
			foreach (LineSegment line in LineSegments)
			{
				_eventPoints.Add(new EventPoint { Point = line.Start, LineSegment = line });
				_eventPoints.Add(new EventPoint { Point = line.End, LineSegment = line });
			}

			while (_eventPoints.Count > 0)
			{
				List<EventPoint> eventPointsList = new List<EventPoint>();

				EventPoint minEventPoint = _eventPoints.Min;
				while (_eventPoints.Count > 0 && _eventPoints.Min.Point.X == minEventPoint.Point.X)
				{
					minEventPoint = _eventPoints.Min;

					eventPointsList.Add(minEventPoint);
					_eventPoints.Remove(minEventPoint);
				}

				_sweepingLineComparer.SweepingLinePosition = minEventPoint.Point.X - 1e-12;

				foreach (EventPoint p in eventPointsList)
				{
					Point currentPoint = p.Point;
					LineSegment currentLineSegment = p.LineSegment, intersectingLineSegment = p.IntersectingLineSegment;

					if (currentPoint == currentLineSegment.Start)
					{
						_sweepingLineStatus.Add(currentLineSegment);
					}
					else if (currentPoint == currentLineSegment.End)
					{
						LineSegment aboveLineSegment = _sweepingLineStatus.GetAbove(currentLineSegment);
						LineSegment belowLineSegment = _sweepingLineStatus.GetBelow(currentLineSegment);

						UpdateIntersectedLines(aboveLineSegment, belowLineSegment);

						_sweepingLineStatus.Remove(currentLineSegment);
					}
					else
					{
						LineSegment aboveLineSegment = currentLineSegment;
						LineSegment belowLineSegment = intersectingLineSegment;

						// Need to swap aboveLineSegment and belowLineSegment
						_sweepingLineStatus.Remove(belowLineSegment);
						_sweepingLineStatus.Remove(aboveLineSegment);

						// Temporary solution set sweeping line position a bit right than event point
						// in order to correclty add segments
						_sweepingLineComparer.SweepingLinePosition = minEventPoint.Point.X + 1e-12;

						_sweepingLineStatus.Add(aboveLineSegment);
						_sweepingLineStatus.Add(belowLineSegment);
					}
				}

				_sweepingLineComparer.SweepingLinePosition = minEventPoint.Point.X + 1e-12;

				foreach (EventPoint p in eventPointsList)
				{
					Point currentPoint = p.Point;
					LineSegment currentLineSegment = p.LineSegment, intersectingLineSegment = p.IntersectingLineSegment;

					if (currentPoint == currentLineSegment.Start)
					{
						LineSegment aboveLineSegment = _sweepingLineStatus.GetAbove(currentLineSegment);
						LineSegment belowLineSegment = _sweepingLineStatus.GetBelow(currentLineSegment);

						UpdateIntersectedLines(aboveLineSegment, currentLineSegment);
						UpdateIntersectedLines(currentLineSegment, belowLineSegment);
					}
					else if (currentPoint == currentLineSegment.End)
					{
					}
					else
					{
						LineSegment aboveLineSegment = intersectingLineSegment;
						LineSegment belowLineSegment = currentLineSegment;
						LineSegment aboveAboveLineSegment = _sweepingLineStatus.GetAbove(aboveLineSegment);
						LineSegment belowBelowLineSegment = _sweepingLineStatus.GetBelow(belowLineSegment);

						UpdateIntersectedLines(aboveAboveLineSegment, aboveLineSegment);
						UpdateIntersectedLines(belowLineSegment, belowBelowLineSegment);
					}
				}

				while (_intersectionEventPoints.Count > 0)
				{
					EventPoint intersectionEventPoint = _intersectionEventPoints.Dequeue();

					if (!_eventPoints.Contains(intersectionEventPoint))
					{
						yield return intersectionEventPoint.Point;

						_eventPoints.Add(intersectionEventPoint);
					}
				}
			}
		}

		private void UpdateIntersectedLines(LineSegment current, LineSegment other)
		{
			if (current != null && other != null && ShewchukPredicatesWrapper.DoLineSegmentsIntersect(current, other))
			{
				Point intersectionPoint = current.FindIntersectionPoint(other);
				if (intersectionPoint != null && intersectionPoint.X >= _sweepingLineComparer.SweepingLinePosition)
				{
					_intersectionEventPoints.Enqueue(new EventPoint { Point = intersectionPoint, LineSegment = current, IntersectingLineSegment = other });
				}
			}
		}

		class EventPoint
		{
			public Point Point { get; set; }
			public LineSegment LineSegment { get; set; }
			public LineSegment IntersectingLineSegment { get; set; }
		}

		class EventPointComparer : IComparer<EventPoint>
		{
			public int Compare(EventPoint a, EventPoint b)
			{
				int xComparisonResult = a.Point.X.CompareTo(b.Point.X);
				if (xComparisonResult != 0)
				{
					return xComparisonResult;
				}

				return a.Point.Y.CompareTo(b.Point.Y);
			}
		}

		class SweepingLineComparer : IComparer<LineSegment>
		{
			public double SweepingLinePosition { get; set; }

			public int Compare(LineSegment a, LineSegment b)
			{
				// Use line equation by two points
				double aIntersectionY = (SweepingLinePosition - a.Start.X) * (a.End.Y - a.Start.Y) / (a.End.X - a.Start.X) + a.Start.Y;
				double bIntersectionY = (SweepingLinePosition - b.Start.X) * (b.End.Y - b.Start.Y) / (b.End.X - b.Start.X) + b.Start.Y;

				return aIntersectionY.CompareTo(bIntersectionY);
			}
		}
	}
}
