﻿using ComputationalGeometry.Algorithm;
using ComputationalGeometry.Geometry;
using NUnit.Framework;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace ComputationalGeometry.Tests
{
	[Category("Intersection")]
	[TestFixture]
	public class IntersectionTests
	{
		private readonly IntersectionAlgorithm _stupidIntersectionAlgorithm = new StupidIntersectionAlgorithm();
		private readonly IntersectionAlgorithm _bentleyOttmannAlgorithm = new BentleyOttmannAlgorithm();
		private readonly List<List<Point>> _stupidResults = new List<List<Point>>();
		private readonly List<List<Point>> _bentleyOttmannResults = new List<List<Point>>();

		public static IEnumerable<TestCaseData> TestCases
		{
			get
			{
				yield return new TestCaseData(
					new List<LineSegment>
					{
						new LineSegment { Start = new Point { X = 20, Y = 100 }, End = new Point { X = 300, Y = 100 } },
						new LineSegment { Start = new Point { X = 200, Y = 20 }, End = new Point { X = 250, Y = 200 } },
						new LineSegment { Start = new Point { X = 100, Y = 200 }, End = new Point { X = 250, Y = 20 } },
						new LineSegment { Start = new Point { X = 20, Y = 120 }, End = new Point { X = 300, Y = 120 } },
						new LineSegment { Start = new Point { X = 200, Y = 40 }, End = new Point { X = 250, Y = 220 } },
						new LineSegment { Start = new Point { X = 100, Y = 220 }, End = new Point { X = 250, Y = 40 } }
					});
				yield return new TestCaseData(
					new List<LineSegment>
					{
						new LineSegment { Start = new Point { X = 40, Y = 60 }, End = new Point { X = 100, Y = 50 } },
						new LineSegment { Start = new Point { X = 70, Y = 40 }, End = new Point { X = 130, Y = 80 } },
						new LineSegment { Start = new Point { X = 30, Y = 120 }, End = new Point { X = 80, Y = 70 } },
						new LineSegment { Start = new Point { X = 20, Y = 100 }, End = new Point { X = 300, Y = 100 } },
						new LineSegment { Start = new Point { X = 20, Y = 20 }, End = new Point { X = 190, Y = 200 } },
						new LineSegment { Start = new Point { X = 200, Y = 20 }, End = new Point { X = 250, Y = 200 } },
						new LineSegment { Start = new Point { X = 100, Y = 200 }, End = new Point { X = 250, Y = 20 } }
					});
				yield return new TestCaseData(
					new List<LineSegment>
					{
						new LineSegment { Start = new Point { X = 20, Y = 20 }, End = new Point { X = 120, Y = 20 } },
						new LineSegment { Start = new Point { X = 20, Y = 40 }, End = new Point { X = 120, Y = 40 } },
						new LineSegment { Start = new Point { X = 20, Y = 60 }, End = new Point { X = 120, Y = 60 } },
						new LineSegment { Start = new Point { X = 20, Y = 80 }, End = new Point { X = 120, Y = 80 } },
						new LineSegment { Start = new Point { X = 30, Y = 10 }, End = new Point { X = 50, Y = 90 } },
						new LineSegment { Start = new Point { X = 50, Y = 10 }, End = new Point { X = 70, Y = 90 } },
						new LineSegment { Start = new Point { X = 70, Y = 10 }, End = new Point { X = 90, Y = 90 } },
						new LineSegment { Start = new Point { X = 90, Y = 10 }, End = new Point { X = 110, Y = 90 } }
					});
				yield return new TestCaseData(
					new List<LineSegment>
					{
						new LineSegment { Start = new Point { X = 10, Y = 10 }, End = new Point { X = 50, Y = 50 } },
						new LineSegment { Start = new Point { X = 10, Y = 20 }, End = new Point { X = 50, Y = 20 } },
						new LineSegment { Start = new Point { X = 30, Y = 10 }, End = new Point { X = 70, Y = 50 } },
						new LineSegment { Start = new Point { X = 30, Y = 40 }, End = new Point { X = 70, Y = 40 } }
					});
				yield return new TestCaseData(
					new List<LineSegment>
					{
						new LineSegment { Start = new Point { X = 20, Y = 100 }, End = new Point { X = 50, Y = 20 } },
						new LineSegment { Start = new Point { X = 40, Y = 20 }, End = new Point { X = 70, Y = 100 } },
						new LineSegment { Start = new Point { X = 20, Y = 50 }, End = new Point { X = 80, Y = 80 } },
						new LineSegment { Start = new Point { X = 20, Y = 80 }, End = new Point { X = 80, Y = 70 } }
					});
			}
		}

		[TestCaseSource(nameof(TestCases))]
		[Order(1)]
		public void StupidIntersectionTest(List<LineSegment> lineSegments)
		{
			_stupidIntersectionAlgorithm.LineSegments = new Collection<LineSegment>(lineSegments);

			_stupidResults.Add(_stupidIntersectionAlgorithm.Run().ToList());
		}

		[TestCaseSource(nameof(TestCases))]
		[Order(2)]
		public void BentleyOttmannIntersectionTest(List<LineSegment> lineSegments)
		{
			_bentleyOttmannAlgorithm.LineSegments = new Collection<LineSegment>(lineSegments);

			_bentleyOttmannResults.Add(_bentleyOttmannAlgorithm.Run().ToList());
		}

		[TestCase]
		public void CompareResults()
		{
			Assert.AreEqual(_stupidResults.Count, _bentleyOttmannResults.Count);

			for (int i = 0; i < _stupidResults.Count; i++)
			{
				Assert.AreEqual(_stupidResults[i].Count / 2, _bentleyOttmannResults[i].Count);
			}
		}

	}
}
