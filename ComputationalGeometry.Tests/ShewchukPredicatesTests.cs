﻿using ComputationalGeometry.Geometry;
using ComputationalGeometry.Randomizer;
using ComputationalGeometry.ShewchukPredicates;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ComputationalGeometry.Tests
{
	[Category("ShewchukPredicates")]
	[TestFixture]
	class ShewchukPredicatesTests
	{
		private const double height = 1000, width = 1000;
		private static readonly GeometryRandomizer _randomizer = new GeometryRandomizer(width, height);

		public static IEnumerable<TestCaseData> IntersectionTestCases
		{
			get
			{
				yield return new TestCaseData(
					new LineSegment { Start = new Point { X = 10, Y = 10 }, End = new Point { X = 100, Y = 100 } },
					new LineSegment { Start = new Point { X = 100, Y = 10 }, End = new Point { X = 10, Y = 100 } }).Returns(true);
				yield return new TestCaseData(
					new LineSegment { Start = new Point { X = 10, Y = 50 }, End = new Point { X = 100, Y = 50 } },
					new LineSegment { Start = new Point { X = 50, Y = 10 }, End = new Point { X = 50, Y = 100 } }).Returns(true);
				yield return new TestCaseData(
					new LineSegment { Start = new Point { X = 10, Y = 10 }, End = new Point { X = 100, Y = 100 } },
					new LineSegment { Start = new Point { X = 10, Y = 300 }, End = new Point { X = 100, Y = 200 } }).Returns(false);
				yield return new TestCaseData(
					new LineSegment { Start = new Point { X = 10, Y = 10 }, End = new Point { X = 100, Y = 10 } },
					new LineSegment { Start = new Point { X = 10, Y = 100 }, End = new Point { X = 100, Y = 100 } }).Returns(false);
				yield return new TestCaseData(
					new LineSegment { Start = new Point { X = 10, Y = 10 }, End = new Point { X = 10, Y = 100 } },
					new LineSegment { Start = new Point { X = 100, Y = 10 }, End = new Point { X = 100, Y = 100 } }).Returns(false);
			}
		}
		public static IEnumerable<TestCaseData> SortByPolarAngleTestCases
		{
			get
			{
				yield return new TestCaseData(10);
				yield return new TestCaseData(10);
				yield return new TestCaseData(10);
				yield return new TestCaseData(100);
				yield return new TestCaseData(200);
			}
		}
		public static IEnumerable<TestCaseData> IsLeftTurnTestCases
		{
			get
			{
				yield return new TestCaseData(
					new Point { X = 300, Y = 10 },
					new Point { X = 100, Y = 100 },
					new Point { X = 400, Y = 200 }).Returns(false);
				yield return new TestCaseData(
					new Point { X = 300, Y = 10 },
					new Point { X = 300, Y = 100 },
					new Point { X = 300, Y = 200 }).Returns(false);
				yield return new TestCaseData(
					new Point { X = 300, Y = 10 },
					new Point { X = 500, Y = 100 },
					new Point { X = 400, Y = 200 }).Returns(true);
			}
		}

		[TestCaseSource(nameof(IntersectionTestCases))]
		public bool DoLineSegmentsIntersectTest(LineSegment a, LineSegment b)
		{
			return ShewchukPredicatesWrapper.DoLineSegmentsIntersect(a, b);
		}

		[TestCaseSource(nameof(SortByPolarAngleTestCases))]
		public void SortByPolarAngleTest(int pointsAmount)
		{
			List<Point> points = _randomizer.NextPoints(pointsAmount).ToList();

			Point minPoint = points.FirstOrDefault();
			foreach (Point p in points)
			{
				if (p.Y < minPoint.Y || p.Y == minPoint.Y && p.X < minPoint.X)
				{
					minPoint = p;
				}
			}

			List<Point> sortedWithAtan2Points = points.OrderBy(p => Math.Atan2(p.Y - minPoint.Y, p.X - minPoint.X)).ToList();

			IEnumerable<Point> minPointEnumerable = new List<Point> { minPoint };
			List<Point> sortedWithShewchukPredicatesPoints = minPointEnumerable.Concat(
				points.Except(minPointEnumerable).OrderBy(p => p, ShewchukPredicatesWrapper.CreateGrahamComparer(minPoint))).ToList();

			System.Diagnostics.Debug.WriteLine($"Sorted with Atan2: {string.Join(", ", sortedWithAtan2Points.Select(p => $"({p.X} {p.Y})"))}");
			System.Diagnostics.Debug.WriteLine($"Sorted with Shewchuk predicates: {string.Join(", ", sortedWithShewchukPredicatesPoints.Select(p => $"({p.X} {p.Y})"))}");

			CollectionAssert.AreEqual(sortedWithAtan2Points, sortedWithShewchukPredicatesPoints);
		}

		[TestCaseSource(nameof(IsLeftTurnTestCases))]
		public bool IsLeftTurnTest(Point a, Point b, Point c)
		{
			return ShewchukPredicatesWrapper.IsLeftTurn(a, b, c);
		}
	}
}
