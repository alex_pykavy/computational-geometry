﻿using ComputationalGeometry.Geometry;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ComputationalGeometry.Tests
{
	[TestFixture]
	public class RectangleTests
	{
		public static IEnumerable<TestCaseData> TestCases
		{
			get
			{
				yield return new TestCaseData(new Rectangle { X1 = 10, Y1 = 10, X2 = 20, Y2 = 20 }, new Rectangle { X1 = 30, Y1 = 10, X2 = 40, Y2 = 20 }).Returns(Tuple.Create(false, false));
				yield return new TestCaseData(new Rectangle { X1 = 10, Y1 = 10, X2 = 20, Y2 = 20 }, new Rectangle { X1 = 10, Y1 = 30, X2 = 20, Y2 = 40 }).Returns(Tuple.Create(false, false));
				yield return new TestCaseData(new Rectangle { X1 = 10, Y1 = 10, X2 = 20, Y2 = 20 }, new Rectangle { X1 = 15, Y1 = 15, X2 = 25, Y2 = 25 }).Returns(Tuple.Create(true, true));
				yield return new TestCaseData(new Rectangle { X1 = 10, Y1 = 10, X2 = 20, Y2 = 20 }, new Rectangle { X1 = 5, Y1 = 5, X2 = 15, Y2 = 15 }).Returns(Tuple.Create(true, true));
				yield return new TestCaseData(new Rectangle { X1 = 10, Y1 = 10, X2 = 30, Y2 = 30 }, new Rectangle { X1 = 15, Y1 = 15, X2 = 25, Y2 = 25 }).Returns(Tuple.Create(true, true));
			}
		}

		[TestCaseSource(nameof(TestCases))]
		public Tuple<bool, bool> DoRectanglesIntersect(Rectangle a, Rectangle b)
		{
			return Tuple.Create(a.Intersects(b), b.Intersects(a));
		}
	}
}
