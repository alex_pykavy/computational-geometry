﻿using ComputationalGeometry.Algorithm;
using ComputationalGeometry.Geometry;
using ComputationalGeometry.Randomizer;
using NUnit.Framework;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace ComputationalGeometry.Tests
{
	[Category("Localization")]
	[TestFixture]
	public class LocalizationTests
	{
		private const int _width = 1000, _heiht = 1000;
		private const int _maxNodeCapacity = 3;
		private const int _pointsAmount = 1000000;
		private readonly GeometryRandomizer _randomizer = new GeometryRandomizer(_heiht, _width);
		private readonly LocalizationAlgorithm _stupidLocalizationAlgorithm = new StupidLocalizationAlgorithm(_heiht, _width);
		private readonly LocalizationAlgorithm _quadTreeLocalizationAlgorithm = new QuadTreeAlgorithm(_maxNodeCapacity, _heiht, _width);
		private readonly List<List<Point>> _stupidResults = new List<List<Point>>();
		private readonly List<List<Point>> _quadTreeResults = new List<List<Point>>();

		public static IEnumerable<TestCaseData> TestCases
		{
			get
			{
				yield return new TestCaseData(new Rectangle { X1 = 10, Y1 = 10, X2 = 20, Y2 = 20 });
				yield return new TestCaseData(new Rectangle { X1 = 10, Y1 = 10, X2 = 100, Y2 = 100 });
				yield return new TestCaseData(new Rectangle { X1 = 10, Y1 = 10, X2 = 1000, Y2 = 1000 });
				yield return new TestCaseData(new Rectangle { X1 = 100, Y1 = 100, X2 = 200, Y2 = 200 });
				yield return new TestCaseData(new Rectangle { X1 = 10, Y1 = 10, X2 = 200, Y2 = 20 });
				yield return new TestCaseData(new Rectangle { X1 = 10, Y1 = 10, X2 = 20, Y2 = 200 });
				yield return new TestCaseData(new Rectangle { X1 = 800, Y1 = 800, X2 = 1000, Y2 = 1000 });
				yield return new TestCaseData(new Rectangle { X1 = 500, Y1 = 500, X2 = 600, Y2 = 700 });
				yield return new TestCaseData(new Rectangle { X1 = 300, Y1 = 500, X2 = 800, Y2 = 700 });
			}
		}

		[OneTimeSetUp]
		public void SetUp()
		{
			List<Point> points = _randomizer.NextPoints(_pointsAmount).ToList();

			_stupidLocalizationAlgorithm.Points = new Collection<Point>(points);
			_quadTreeLocalizationAlgorithm.Points = new Collection<Point>(points);

			_stupidLocalizationAlgorithm.Prepare();
			_quadTreeLocalizationAlgorithm.Prepare();
		}

		[TestCaseSource(nameof(TestCases))]
		[Order(1)]
		public void StupidLocalizationTest(Rectangle region)
		{
			_stupidLocalizationAlgorithm.Region = region;

			_stupidResults.Add(_stupidLocalizationAlgorithm.Run().ToList());
		}

		[TestCaseSource(nameof(TestCases))]
		[Order(2)]
		public void QuadTreeLocalizationTest(Rectangle region)
		{
			_quadTreeLocalizationAlgorithm.Region = region;

			_quadTreeResults.Add(_quadTreeLocalizationAlgorithm.Run().ToList());
		}

		[TestCase]
		public void CompareResults()
		{
			Assert.AreEqual(_stupidResults.Count, _quadTreeResults.Count);

			for (int i = 0; i < _stupidResults.Count; i++)
			{
				Assert.AreEqual(_stupidResults[i].Count, _quadTreeResults[i].Count);
			}
		}
	}
}
